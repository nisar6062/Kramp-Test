# Library API

Provides service to get books and albums.

# Technologies Used
## 1. Spring Boot - Framework for creating the whole application
It uses the features of spring.It can do things automatically but allows you to override the defaults if required.Spring provides a wide variety of features addressing the modern business needs via its portfolio projects.
* Configure beans in multiple ways such as XML, Annotations and JavaConfig.
* Spring’s dependency injection approach encourages writing testable code.
* Easy to use but powerful database transaction management capabilities
* Spring simplifies integration with other Java frameworks like JPA/Hibernate ORM, Struts/JSF/etc. web frameworks
* Easy annotation based configurations
* Spring Boot has Embedded Apache Tomcat server for easy deployment
* Easy Integration with other APIs
* Provides health check, metrics, guage etc..
* Supports Gradle & maven

## 2. Gradle - For Building the project
Gradle is considered to be a better choice than maven because of the features below
* Incremental build
* Task Output Caching - cache is build to reuse task output
* Compiler Daemon - speeds up the build
* Easy conflict resolution - considers the latest version
* Better metadata resolution

## 3. Git - For Version Control
* Best in the market and opensource
* Easy setup and command prompt enabled

## 4. Checkstyle & PMD - For Better Coding Style
Checkstyle, PMD has a set of rule which can enabled/disabled using the xml configurations.These rules helps in cleaning and formating the code.

## 5. Import Control - For Better Control of Class/Package Imports
Helps to maintain the imports using xml configurations.

## 6. Find Bugs - For catching bad pratices
Helps to maintain a better code

# Setup the Application

#### 1. Install Java 8
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
#### 2. Install Git
```
$ sudo apt-get install git
```
#### 3. Install Gradle
```
$ sudo apt-get install gradle
```
#### 4. Install Docker
https://docs.docker.com/engine/installation/#supported-platforms

### 5. Clone from the Git Lab Repository
```
$ git clone https://gitlab.com/nisar6062/Kramp-Test.git -b master
$ cd Kramp-Test
```
### 6. Build the Project 
```
$ gradle clean build
```

### 7. Run with Application
#### 7.1 Run with Gradle
```
$ gradle bootrun -Dspring.profiles.active=local
```
change the profiles to local,stage or prod
Either bootun or docker run can be used for running application

#### 7.2 Run with Docker
```
# create the image
$ ./gradlew clean buildDocker
# run the image 
$ docker run -p 8080:8080 -e spring.profiles.active=local kramp/kramp-test:1.0.0
# get all running Docker containers
$ docker ps
# stop
$ docker stop <container-id>
# delete
docker rm <container-id>
```

## 8.Access the API

## Use Swagger
```
http://<host>:<port>/swagger-ui.html
eg: http://localhost:8080/swagger-ui.html
```
## Use Postman to access the API
### Health check API
```
http://<host>:<port>/actuators/health
eg: http://localhost:8080/actuators/health
```
### Metrics API
```
http://<host>:<port>/actuators/metrics
eg: http://localhost:8080/actuators/metrics
```
### Library API
```
http://<host>:<port>/library/search?term=<input>
eg: http://localhost:8080/library/search?term=good
```
## 9. Configurable Properties
In the folder src/main/resources/config there a three property files where the user can configure their properties based on the environment which they are running.
For eg: if you are running your application in production, you will be using the command
gradle bootrun -Dspring.profiles.active=prod
which means that it reads the file application-prod.properties.
It has properties like albumurl, bookurl and maximum number of records needs to be shown in the response for album & book.

## 10. JavaDoc
The javadoc is pushed to the git.Once the project is cloned, open the index.html in the doc folder. To Generate javadoc from eclipse go to Project -> Generate JavaDoc.

## 11. Application Logs
Logs will be there in the project main folder fileName: 'kramp-test-app.log'
