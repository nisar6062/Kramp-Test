package com.library.model;

import java.util.List;
/**
 * LibraryResponse POJO class
 * @author nisar.adappadathil
 **/
public class LibraryResponse {
    private String message;
    private List<Library> libraryList;
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Library> getLibraryList() {
        return libraryList;
    }

    public void setLibraryList(List<Library> libraryList) {
        this.libraryList = libraryList;
    }

}
