
package com.library.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Book POJO class
 * @author nisar.adappadathil
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {
    private String kind;
    private int totalItems;
    private List<BookItem> items;
    
    public String getKind() {
        return kind;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public int getTotalItems() {
        return totalItems;
    }
    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }
    
    public List<BookItem> getItems() {
        return items;
    }
    public void setItems(List<BookItem> items) {
        this.items = items;
    }
    @Override
    public String toString() {
        return "Book [kind=" + kind + ", totalItems=" + totalItems + ", itemList=" + items + "]";
    }
   
}
