package com.library.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/**
 * AlbumResult POJO class
 * @author nisar.adappadathil
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlbumResult {
    private String artistName;
    private String trackName;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    @Override
    public String toString() {
        return "AlbumResult [artistName=" + artistName + ", trackName=" + trackName + "]";
    }

}
