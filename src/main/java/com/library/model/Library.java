package com.library.model;

import java.util.List;
/**
 * Library POJO class
 * @author nisar.adappadathil
 **/
public class Library implements Comparable<Library> {
    private String title;
    private List<String> authors;
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "Library [title=" + title + ", authors=" + authors + ", type=" + type + "]";
    }

    @Override
    public int compareTo(Library library) {
        if (library.getType().equals(this.getType())) {
            if (library.getTitle() == null || this.getTitle() == null) {
                if (library.getTitle() == null && this.getTitle() == null) {
                    return 0;
                }
                if (library.getTitle() == null && this.getTitle() != null) {
                    return -1;
                }
                if (library.getTitle() != null && this.getTitle() == null) {
                    return 1;
                }
            }
            if (library.getTitle().compareTo(this.getTitle()) < 0) {
                return 1;
            }
            if (library.getTitle().compareTo(this.getTitle()) > 0) {
                return -1;
            }
        }
        if (library.getType().compareTo(this.getType()) < 0) {
            return 1;
        }
        if (library.getType().compareTo(this.getType()) > 0) {
            return -1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((authors == null) ? 0 : authors.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Library other = (Library) obj;
        if (authors == null) {
            if (other.authors != null) {
                return false;
            }
        } else if (!authors.equals(other.authors)) {
            return false;
        }
        if (title == null) {
            if (other.title != null) {
                return false;
            }
        } else if (!title.equals(other.title)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            } 
        } else if (!type.equals(other.type)) {
            return false;
        }
        return true;
    }
}
