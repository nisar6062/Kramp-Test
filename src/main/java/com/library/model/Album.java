package com.library.model;

import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Album POJO class
 * @author nisar.adappadathil
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {
    List<AlbumResult> results;

    public List<AlbumResult> getResults() {
        return results;
    }

    public void setResults(List<AlbumResult> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Album [results=" + results + "]";
    }
}
