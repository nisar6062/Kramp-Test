package com.library.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/**
 * VolumeInfo POJO class
 * @author nisar.adappadathil
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class VolumeInfo {
    private String title;
    private List<String> authors;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
          return authors;
     }

     public void setAuthors(List<String> authors) {
          this.authors = authors;
     }

     @Override
     public String toString() {
          return "VolumeInfo [title=" + title + ", authors=" + authors + "]";
     }

}
