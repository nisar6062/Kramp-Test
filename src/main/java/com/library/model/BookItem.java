package com.library.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
/**
 * BookItem POJO class
 * @author nisar.adappadathil
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookItem {
    private String kind;
    private VolumeInfo volumeInfo;
    public String getKind() {
        return kind;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }
    public void setVolumeInfo(VolumeInfo volumeInfo) {
        this.volumeInfo = volumeInfo;
    }
    @Override
    public String toString() {
        return "BookItem [kind=" + kind + ", volumeInfo=" + volumeInfo + "]";
    }
    
}
