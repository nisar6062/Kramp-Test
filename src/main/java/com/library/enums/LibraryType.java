package com.library.enums;
/**
 * Enum class defines various types of item
 * @author nisar.adappadathil
 **/
public enum LibraryType {
    BOOK("book"), ALBUM("album");
    private final String text;

    /**
     * @param text
     */
    LibraryType(final String text) {
        this.text = text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
