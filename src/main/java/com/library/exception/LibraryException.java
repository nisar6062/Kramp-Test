package com.library.exception;
/**
 * Custom Excpetion handling class
 * @author nisar.adappadathil
 **/
public class LibraryException extends Exception {
    private static final long serialVersionUID = -7822908006818878634L;

    public LibraryException(String message) {
        super(message);
    }
}
