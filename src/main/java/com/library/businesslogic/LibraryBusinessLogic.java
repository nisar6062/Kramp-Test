package com.library.businesslogic;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Service;

import com.library.dao.LibraryDao;
import com.library.exception.LibraryException;
import com.library.model.Album;
import com.library.model.Book;
import com.library.model.Library;
import com.library.model.LibraryResponse;
import com.library.utils.LibraryUtils;
/**
 * Business logic Class to handle custom logic
 * @author nisar.adappadathil
 **/
@Service
public class LibraryBusinessLogic {

    private static final Logger logger = LoggerFactory.getLogger(LibraryBusinessLogic.class);
    @Autowired
    LibraryDao libraryDAO;
    @Autowired
    private LibraryUtils utils;
    private final GaugeService gauges;
    public LibraryBusinessLogic(GaugeService gauges) {
        this.gauges = gauges;
    }
    
    public String healthCheck() {
        String encodedValue = null;
        if (!netIsAvailable()) {
           return "Check the internet connection";
        }
        try {
            encodedValue = URLEncoder.encode("Good", java.nio.charset.StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            return "Parsing Of Input Error";
        }
        StringBuilder message = new StringBuilder(50);
        
        try {
            libraryDAO.getBooks(encodedValue);
        } catch (Exception e) {
            logger.error(e.getMessage());
            message.append("Book API is Down");
        }
        try {
            libraryDAO.getAlbums(encodedValue);
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (!StringUtils.isBlank(message.toString())) {
                message.append(" & ");
            }
            message.append("Album API is Down");
        }
        return message.toString();
    }
    
    public LibraryResponse getBooksnAlbums(String term) throws LibraryException {
        logger.info("In Business logic layer");
        if (!netIsAvailable()) {
            throw new LibraryException("Check the internet connection");
         }
        if (StringUtils.isBlank(term)) {
            logger.error("The input should not be blank");
            throw new LibraryException("The input should not be blank");
        }
        String encodedValue = null;
        long start = System.currentTimeMillis();
        try {
            encodedValue = URLEncoder.encode(term, java.nio.charset.StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            throw new LibraryException("Exception In parsing the input Exception: " + e);
        }
        Book book = null;
        Album album = null;
        StringBuilder message = new StringBuilder(72);
        try {
             book = libraryDAO.getBooks(encodedValue);
             if (book.getItems().size() == 0) {
                 message.append("No Books Found ");
             }
        } catch (Exception e) {
            logger.error(e.getMessage());
            message.append("Book API is down ");
        }
        this.gauges.submit("timer.library.bookAPI", (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        try {
            album = libraryDAO.getAlbums(encodedValue);
            if (album.getResults().size() == 0) {
                message.append("and No Albums Found");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            message.append("and Album API is down");
        }
        this.gauges.submit("timer.library.albumAPI", (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        List<Library> libraryList = new ArrayList<Library>();
        if (book != null) {
            libraryList.addAll(utils.convertBookToLibraryList(book));
        }
        if (album != null) {
            libraryList.addAll(utils.convertAlbumToLibraryList(album));
        }
        Collections.sort(libraryList);
        
        logger.info("library JSON", libraryList);
        LibraryResponse libraryResponse = new LibraryResponse();
        libraryResponse.setMessage(message.toString());
        libraryResponse.setLibraryList(libraryList);
        this.gauges.submit("timer.library.dataFormating", (System.currentTimeMillis() - start));
        return libraryResponse;
    }
    private boolean netIsAvailable() {
        try {
            libraryDAO.accessRESTAPI("http://www.google.com", "");
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
