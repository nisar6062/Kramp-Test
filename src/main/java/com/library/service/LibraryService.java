package com.library.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.library.businesslogic.LibraryBusinessLogic;
import com.library.model.LibraryResponse;

import io.swagger.annotations.ApiOperation;

/**
 * Service class for handling all CRUD Operations.
 * 
 * @author nisar.adappadathil
 */
@Service
@RequestMapping("/library/")
public class LibraryService {
    private static final Logger logger = LoggerFactory.getLogger(LibraryService.class);
    @Autowired
    LibraryBusinessLogic bussLogic;
    private final GaugeService gauges;
    public LibraryService(GaugeService gauges) {
        this.gauges = gauges;
    }
    @ApiOperation(value = "Search For Albums and Books", notes = "search by entering the term",
               response = LibraryResponse.class)
    @RequestMapping(value = "search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> getBooksnAlbums(@RequestParam("term") String term) {
        logger.info("get Books and Albums from dictionary");
        try {
            long start = System.currentTimeMillis();
            LibraryResponse reponse = bussLogic.getBooksnAlbums(term);
            this.gauges.submit("timer.library.search", (System.currentTimeMillis() - start));
            return new ResponseEntity<Object>(reponse, HttpStatus.OK);
        } catch (Exception e) {
            logger.debug("Exception Occured", e.getMessage());
            return new ResponseEntity<Object>(e, HttpStatus.BAD_REQUEST);
        }
    }
}
