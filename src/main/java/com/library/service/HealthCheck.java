package com.library.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.library.businesslogic.LibraryBusinessLogic;
/**
 * Health check service class
 * @author nisar.adappadathil
 **/
@Component
public class HealthCheck implements HealthIndicator {
    @Autowired
    LibraryBusinessLogic bussLogic;
    @Override
    public Health health() {
        String errorMsg = check(); // perform some specific health check
        if (!StringUtils.isBlank(errorMsg)) {
            return Health.down().status(HttpStatus.BAD_REQUEST.toString())
              .withDetail("Error Message", errorMsg).build();
        }
        return Health.up().status(HttpStatus.OK.toString()).withDetail("Message", "Album API & Book API is up").build();
    }
    
    public String check() {
        // Your logic to check health
        return bussLogic.healthCheck();
    } 
}
