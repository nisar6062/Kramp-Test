
package com.library.dao;

import java.util.stream.Collectors;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.library.businesslogic.LibraryBusinessLogic;
import com.library.model.Album;
import com.library.model.Book;
import com.library.utils.PropertyConfiguration;
/**
 * Dao Implementation class for handling get Operation.
 * @author nisar.adappadathil
 **/
@Repository
public class LibraryDaoImpl implements LibraryDao {
    private static final Logger logger = LoggerFactory.getLogger(LibraryBusinessLogic.class);
    @Autowired
    private PropertyConfiguration config;
    private final ObjectMapper mapper = new ObjectMapper();
    
    @Override
    public Book getBooks(String encodedValue) throws Exception {
        Book book = mapper.readValue(accessRESTAPI(config.getBookURL(), encodedValue), Book.class);
        book.setItems(book.getItems().stream().limit(config.getMaxNumberOfBooks()).collect(Collectors.toList()));
        return book;
    }
    
    @Override
    public Album getAlbums(String encodedValue) throws Exception {
        Album album = mapper.readValue(accessRESTAPI(config.getAlbumURL(), encodedValue), Album.class);
        album.setResults(album.getResults().stream().limit(config.getMaxNumberOfAlbums()).collect(Collectors.toList()));
        return album;
    }
    @Override
    public String accessRESTAPI(String uri, String term) {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri.concat(term), String.class);
        logger.info("Book JSON", result);
        return result;
    }

}
