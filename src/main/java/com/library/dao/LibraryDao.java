
package com.library.dao;

import javax.annotation.Nonnull;

import com.library.model.Album;
import com.library.model.Book;

/**
 * Dao interface for all data access operations
 * 
 * @author nisar.adappadathil
 */
public interface LibraryDao {

    /**
     * @param term
     * @return {@link Book}
     */
     @Nonnull Book getBooks(String term) throws Exception;
     
     @Nonnull Album getAlbums(String term) throws Exception;
     
     @Nonnull String accessRESTAPI(String uri, String term);
}
