package com.library.dao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
/**
 * Config Class to scan the whole package
 * @author nisar.adappadathil
 **/
@ComponentScan
@Configuration
public class DaoConfig {

}
