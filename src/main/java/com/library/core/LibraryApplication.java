package com.library.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.library.businesslogic.BusinessLogicConfig;
import com.library.dao.DaoConfig;
import com.library.service.ServiceConfig;
import com.library.utils.UtilsConfig;
/**
 * Spring Boot Application Class
 * starting point of the application
 * @author nisar.adappadathil
 **/
@SpringBootApplication
@Import({ ServiceConfig.class, DaoConfig.class, UtilsConfig.class, BusinessLogicConfig.class})
public class LibraryApplication {
     private static final Logger logger = LoggerFactory.getLogger(LibraryApplication.class);

     public static void main(String[] args) {
          logger.info("Starting the Application");
          SpringApplication.run(LibraryApplication.class, args);
     }
}
