package com.library.utils;

import java.util.Arrays;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.core.env.Environment;

import com.google.common.annotations.VisibleForTesting;

/**
 * Used via start parameter to define which config files to load.
 * @author nisar.adappadathil
 **/
public enum ApplicationProfile {
    DEV(Constants.DEV_NAME), TEST(Constants.TEST_NAME), LOCAL(Constants.LOCAL_NAME);

   /*
    *  @ActiveProfiles  annotation that is used to declare which active bean definition profiles
    *  should be used when loading an class for test classes.
    *  Use this for methods for fixing issues in test classes
    */
    private final String name;

    ApplicationProfile(String name) {
        this.name = name;
        System.setProperty("app.environment", name);
    }
    
    @VisibleForTesting
    @Nonnull
    static Optional<ApplicationProfile> forName(@Nonnull String name) {
        for (ApplicationProfile profile : ApplicationProfile.values()) {
            if (profile.getName().equalsIgnoreCase(name)) {
                return Optional.of(profile);
            }
        }
        return Optional.empty();
    }

    /**
     * return application profiled defined by spring.profiles.active
     * configuration
     */
    @Nonnull
    public static ApplicationProfile fromSpringEnvironment(@Nonnull Environment environment) {
        ApplicationProfile found = null;
        for (String springProfileName : environment.getActiveProfiles()) {
            Optional<ApplicationProfile> applicationProfile = forName(springProfileName);
            if (applicationProfile.isPresent()) {
                if (found != null) {
                    throw new IllegalStateException(
                            "Ambiguous Spring Application Profile " + found + ", " + applicationProfile.get());
                }
                found = applicationProfile.get();
            }
        }
        if (found == null) {
            throw new IllegalStateException("No Application Profile in Spring environment, only found profiles "
                    + Arrays.toString(environment.getActiveProfiles()) + ", configure spring.profiles.active");
        }
        return found;
    }

    public String getName() {
        return name;
    }

    /**
     * In Spring annotations like @Profile or @ActiveProfile, prefer these
     * constants to String literals, to help with traceability
     */
    public static class Constants {
        public static final String DEV_NAME = "dev";
        public static final String LOCAL_NAME = "local";
        public static final String TEST_NAME = "test";
    }
}
