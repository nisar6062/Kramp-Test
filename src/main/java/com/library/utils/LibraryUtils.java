package com.library.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.library.enums.LibraryType;
import com.library.model.Album;
import com.library.model.Book;
import com.library.model.Library;
/**
 * Utils Class 
 * @author nisar.adappadathil
 **/
@Component
public class LibraryUtils {
    public List<Library> convertBookToLibraryList(Book book) {
        List<Library> libraryList = new ArrayList<Library>();
        book.getItems().forEach(item -> {
            Library library = new Library();
            library.setTitle(item.getVolumeInfo().getTitle());
            library.setAuthors(item.getVolumeInfo().getAuthors());
            library.setType(LibraryType.BOOK.toString());
            libraryList.add(library);
        });
        return libraryList;
    }
    
    public List<Library> convertAlbumToLibraryList(Album album) {
        List<Library> libraryList = new ArrayList<Library>();
        album.getResults().forEach(result -> {
            Library library = new Library();
            library.setTitle(result.getTrackName());
            library.setAuthors(Arrays.asList(result.getArtistName()));
            library.setType(LibraryType.ALBUM.toString());
            libraryList.add(library);
        });
        return libraryList;
    }
}
