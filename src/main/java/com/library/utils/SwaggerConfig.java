package com.library.utils;


import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Nullable;

import static springfox.documentation.builders.RequestHandlerSelectors.withMethodAnnotation;
/**
 * Swagger Config Class 
 * @author nisar.adappadathil
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Nullable
    @Value("${rakuten.itemx.swagger.basepath:#{null}}")
    private String swaggerBasePath;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .description("Search For Albums and Books")
                        .license("Copyright (c) 2017 Nisar, All Rights Reserved.")
                        .title("Kramp-Test")
                        .build())
                .pathMapping(swaggerBasePath)
                .select()
                .apis(withMethodAnnotation(ApiOperation.class))
                .build();
    }
}
