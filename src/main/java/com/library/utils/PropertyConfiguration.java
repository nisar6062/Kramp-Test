package com.library.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
/**
 * Application Config Class 
 * @author nisar.adappadathil
 **/
@Configuration
@PropertySource("classpath:config/application-${spring.profiles.active}.properties")
public class PropertyConfiguration {

    @Autowired
    private Environment env;

    public String getBookURL() {
        return env.getProperty("book.url");
    }
    
    public String getAlbumURL() {
        return env.getProperty("album.url");
    }
    
    public int getMaxNumberOfAlbums() {
        return Integer.parseInt(env.getProperty("max.albums"));
    }
    
    public int getMaxNumberOfBooks() {
        return Integer.parseInt(env.getProperty("max.books"));
    }

}
